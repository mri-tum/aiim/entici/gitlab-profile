### Source Code & Artifact Availability

Currently, the complete source code as well as all associated software artifacts of entici are managed in a private GitLab repository and artifactory, respectively.

Migration of all repositories as well as artifacts to this GitLab group is under development.

In the meantime, in order to access the source code and/or get access to the artifacts, or if you have any questions contact [johanna.eicher@tum.de](mailto:johanna.eicher@tum.de).